package com.Ejercicios_Tecnicos.Dirrecciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirreccionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DirreccionesApplication.class, args);
	}

}
